from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, headers=headers, params=params)
    pictures = json.loads(response.content)
    try:
        return {"picture_url": pictures["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "https://api.openweathermap.org"
    headers = {""}
    response = requests.get(OPEN_WEATHER_API_KEY)
    lon_lat = json.loads(response.content)
    weather = {
        "temperature": lon_lat["temperature"],
        "description": lon_lat["description"],
    }

    return weather
